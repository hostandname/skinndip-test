<?php

   //debugging
   //ini_set("display_errors", 1);
   //error_reporting(E_ALL);

   require 'vendor/autoload.php';

   use Symfony\Component\HttpFoundation\Request;

   $app = new Silex\Application();
   $app['debug'] = true;

   $shopifyClient = new Shopify\Client();
   $shopifyUrl = "skinnydip-test4.myshopify.com";
   $shopifyPassword = "01a728323fba481cf16cbee6a5a5ca03";
   $shopifyClient->settings(Array("shopUrl" => $shopifyUrl, "X-Shopify-Access-Token" => $shopifyPassword));
   $app["shopify_client"] = $shopifyClient;

   $app->register(new Silex\Provider\TwigServiceProvider(), array(
      'twig.path' => __DIR__.'/views',
   ));

   $app->get('/', function() use ($app) {
      return $app["twig"]->render("layout.twig", array(
         "something" => "interesting"
      ));
   });

   $app->get('/sample.json', function() use($app) {
      $sample = Array(
         Array(
            "key1" => "value1 a",
            "key2" => "value2 a",
            "key3" => "value3 a",
            "key4" => "value4 a"
         ),
         Array(
            "key1" => "value1 b",
            "key2" => "value2 b",
            "key3" => "value3 b",
            "key4" => "value4 b"
         )
      );
      return $app->json($sample);
   });

   $app->get('/products.json', function(Request $request) use($app) {

      $page = intval($request->get("page"));
      if (!$page) $page = 1;

      $params = Array("limit" => 20, "page" => $page);
      $shopifyProducts = $app["shopify_client"]->getProducts($params)["products"];

      $product_array = array();

      foreach ($shopifyProducts as $product) {
         foreach($product["variants"] as $variant) {
            $product_array[] = array(
               'title' => $product["title"], // always display the product title
               "barcode" => $variant["barcode"], // product barcode
               "sku" => $variant["sku"] // product SKU
            );
         }// end foreach
      }// end foreach

      return $app->json($product_array);
   });

   $app->run();
